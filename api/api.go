package api

import (
	_ "alzargar_backend/api/docs"
	v1 "alzargar_backend/api/v1"
	"alzargar_backend/config"
	"alzargar_backend/pkg/logger"
	"alzargar_backend/storage"

	"github.com/gin-contrib/cors"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
)

type RouterOptions struct {
	Cfg     *config.Config
	Storage storage.StorageI
	Log     logger.Logger
}

// New
// @title Alzargar
// @version 1.0
// @description Alzargar backend
// @securityDefinitions.apikey ApiKeyAuth
// @BasePath /v1
// @in  header
// @name Authorization
// @host localhost:8080
func New(opt *RouterOptions) *gin.Engine {
	router := gin.New()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1{
		Cfg:  opt.Cfg,
		Strg: &opt.Storage,
		Log:  opt.Log,
	})

	router.Static("/media", "./media")

	apiV1 := router.Group("/v1")

	// auth
	apiV1.POST("/auth/register", handlerV1.Register)
	apiV1.POST("/auth/login", handlerV1.Login)

	// basket
	apiV1.POST("/basket", handlerV1.CreateBasket)
	apiV1.GET("/basket/:id", handlerV1.GetBasketByUserId)
	apiV1.PUT("/bup", handlerV1.UpdateBasket)
	apiV1.DELETE("/bde/:id", handlerV1.DeleteBasket)

	// favorite
	apiV1.POST("/favorite", handlerV1.AddFavoriteProduct)
	apiV1.DELETE("/favorite", handlerV1.RemoveFavorite)
	apiV1.GET("/favorite/:id", handlerV1.GetFavoruteProducts)

	// user
	apiV1.GET("/user", handlerV1.GetById)
	apiV1.DELETE("/user", handlerV1.DeleteUserById)
	apiV1.PUT("/user", handlerV1.UpdateUserById)
	apiV1.GET("/is_login/:token", handlerV1.IsLogin)

	// category
	apiV1.GET("/:id", handlerV1.GetCategoryById)
	apiV1.GET("/category", handlerV1.GetAllCategory)

	// product
	apiV1.POST("/product", handlerV1.CreateProduct)
	apiV1.GET("/product", handlerV1.GetAllProducts)
	apiV1.GET("/product/:id", handlerV1.GetByCategoryId)
	apiV1.GET("/productmm", handlerV1.GetMinMaxPrice)
	apiV1.GET("/products/:min/:max", handlerV1.GeByPrice)
	apiV1.GET("/search/:language/:search", handlerV1.SearchProduct)
	apiV1.DELETE("/product/:id", handlerV1.DeleteProductById)
	apiV1.PUT("/product", handlerV1.UpdateProduct)

	// file-upload
	apiV1.POST("/file-upload", handlerV1.UploadFile)

	url := ginSwagger.URL("swagger/doc.json")
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
