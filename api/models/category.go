package models

type Data struct {
	Link string `json:"link"`
	Name Name   `json:"name"`
}

type Name struct {
	Uz string `json:"uz"`
	Ru string `json:"ru"`
	En string `json:"en"`
}

type CategoryRes struct {
	Id        int    `json:"id"`
	Name      Name   `json:"name"`
	Image_url string `json:"image_url"`
	Link      string `json:"link"`
	Data      []Data `json:"data"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type AllCategory struct {
	Categories []CategoryRes `json:"categories"`
}
