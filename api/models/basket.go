package models

type BasketReqForUpdate struct {
	ID        int `json:"id"`
	Count     int `json:"count"`
	ProductId int `json:"product_id"`
}

type BasketReq struct {
	UserId    int    `json:"user_id"`
	ProductId int    `json:"product_id"`
	Count     int    `json:"counts"`
}

type BasketRes struct {
	ID        int    `json:"id"`
	UserId    int    `json:"user_id"`
	ProductId int    `json:"product_id"`
	Count     int    `json:"counts"`
	CreatedAt string `json:"created_at"`
}

type AllBaskets struct {
	Baskets []BasketRes `json:"baskets"`
}
