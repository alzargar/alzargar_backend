package models

type FavoriteReq struct {
	UserID    int `json:"user_id"`
	ProductID int `json:"product_id"`
}

type FavoriteRes struct {
	UserID    int `json:"user_id"`
	ProductID int `json:"product_id"`
}
