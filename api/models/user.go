package models


type User struct {
	Id            string `json:"id" binding:"required"`
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	Password      string `json:"password" binding:"required"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
}
type UserReq struct {
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	Password      string `json:"password" binding:"required"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
}