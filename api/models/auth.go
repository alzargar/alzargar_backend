package models

type RegisterReqs struct {
	Id            string `json:"id" binding:"required"`
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	Password      string `json:"password" binding:"required"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
	UserType      string `json:"user_type"`
}
type RegisterReq struct {
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	Password      string `json:"password" binding:"required"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
}

type LoginReq struct {
	PhoneNumber string `json:"phone_number" binding:"required"`
	Password    string `json:"password" binding:"required"`
}

type LoginRes struct {
	Id            string `json:"id" binding:"required"`
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
	AccesToken    string `json:"acces_token"`
	ExpiredAt     int64  `json:"expired_at"`
}

type IdRequest struct {
	Id string `json:"id"`
}
