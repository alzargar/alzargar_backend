package models

type ReqForUpdate struct {
	Id            int      `json:"id"`
	Category      Category `json:"category"`
	Title         Language `json:"title"`
	IsDiscount    bool     `json:"is_discount"`
	Price         float64  `json:"price"`
	DiscountPrice float64  `json:"discount_price"`
	Rate          int      `json:"rate"`
	Color         string   `json:"color"`
	Shape         string   `json:"shape"`
	Images        Image    `json:"images"`
	GenderType    string   `json:"gender_type"`
	Type          string   `json:"type"`
	UpdateAt      string   `json:"updated_at"`
}

type ReqCategoryType struct {
	CategoryName string `json:"category_name"`
}

type Search struct {
	Search string `json:"search"`
}

type MinMaxPrice struct {
	MinPrice float64 `json:"min_price"`
	MaxPrice float64 `json:"max_price"`
}

type Category struct {
	Id int `json:"id"`
}

type ProductReq struct {
	Category      Category `json:"category"`
	Title         Language `json:"title"`
	IsDiscount    bool     `json:"is_discount"`
	Price         float64  `json:"price"`
	DiscountPrice float64  `json:"discount_price"`
	Rate          int      `json:"rate"`
	Color         string   `json:"color"`
	Shape         string   `json:"shape"`
	Images        Image    `json:"images"`
	GenderType    string   `json:"gender_type"`
	Type          string   `json:"type"`
}

type Image struct {
	ImageAbove    string `json:"image_above"`
	ImageFromSide string `json:"image_side"`
	ImageFromTag  string `json:"image_tag"`
}

type Language struct {
	TitleUz  string `json:"uz"`
	TitleEng string `json:"en"`
	TitleRu  string `json:"ru"`
}

type ProductRes struct {
	Id            int      `json:"id"`
	Category      Category `json:"category"`
	Title         Language `json:"title"`
	IsDiscount    bool     `json:"is_discount"`
	Price         float64  `json:"price"`
	DiscountPrice float64  `json:"discount_price"`
	Rate          int      `json:"rate"`
	Color         string   `json:"color"`
	Shape         string   `json:"shape"`
	Images        Image    `json:"images"`
	GenderType    string   `json:"gender_type"`
	Type          string   `json:"type"`
	CreatedAt     string   `json:"created_at"`
	UpdatedAt     string   `json:"updated_at"`
}

type AllProducts struct {
	Products []ProductRes
}

type AllProductsUz struct {
	Products []ProductResUz
}
type AllProductsRu struct {
	Products []ProductResRu
}
type AllProductsEn struct {
	Products []ProductResEn
}

type ProductResUz struct {
	Id            int      `json:"id"`
	Category      Category `json:"category"`
	TitleUz       string   `json:"uz"`
	IsDiscount    bool     `json:"is_discount"`
	Price         float64  `json:"price"`
	DiscountPrice float64  `json:"discount_price"`
	Rate          int      `json:"rate"`
	Color         string   `json:"color"`
	Shape         string   `json:"shape"`
	Images        Image    `json:"images"`
	GenderType    string   `json:"gender_type"`
	Type          string   `json:"type"`
	CreatedAt     string   `json:"created_at"`
}

type ProductResRu struct {
	Id            int      `json:"id"`
	Category      Category `json:"category"`
	TitleRu       string   `json:"ru"`
	IsDiscount    bool     `json:"is_discount"`
	Price         float64  `json:"price"`
	DiscountPrice float64  `json:"discount_price"`
	Rate          int      `json:"rate"`
	Color         string   `json:"color"`
	Shape         string   `json:"shape"`
	Images        Image    `json:"images"`
	GenderType    string   `json:"gender_type"`
	Type          string   `json:"type"`
	CreatedAt     string   `json:"created_at"`
}

type ProductResEn struct {
	Id            int      `json:"id"`
	Category      Category `json:"category"`
	TitleEn       string   `json:"en"`
	IsDiscount    bool     `json:"is_discount"`
	Price         float64  `json:"price"`
	DiscountPrice float64  `json:"discount_price"`
	Rate          int      `json:"rate"`
	Color         string   `json:"color"`
	Shape         string   `json:"shape"`
	Images        Image    `json:"images"`
	GenderType    string   `json:"gender_type"`
	Type          string   `json:"type"`
	CreatedAt     string   `json:"created_at"`
}
