package v1

import (
	"alzargar_backend/api/models"
	"errors"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Router /v1/file-upload [post]
// @Summary File upload
// @Description File upload
// @Tags file-upload
// @Accept json
// @Produce json
// @Param file formData file true "File"
// @Success 200 {object} string
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
func (h *handlerV1) UploadFile(c *gin.Context) {
	var file File

	if err := c.ShouldBind(&file); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}
	if filepath.Ext(file.File.Filename) != ".png" && filepath.Ext(file.File.Filename) != ".jpg" && filepath.Ext(file.File.Filename) != ".svg" && filepath.Ext(file.File.Filename) != ".jpeg" {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, errors.New(models.MessageErrorSmthng))
		return
	}
	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dir, _ := os.Getwd()

	if _, err := os.Stat(dir + "/media"); os.IsNotExist(err) {
		os.Mkdir(dir+"/media", os.ModePerm)
	}

	filePath := "/media/" + fileName
	err := c.SaveUploadedFile(file.File, dir+filePath)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"filename" : c.Request.Host + filePath,
	})
}
