package v1

import (
	"alzargar_backend/api/models"
	"alzargar_backend/pkg/utils"
	"alzargar_backend/storage/repo"
	"context"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Get user
// @Summary User
// @Security ApiKeyAuth
// @Tags user
// @Description This api can get user by id
// @Accept json
// @Produce json
// @Succes 200 {object}  models.User
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/user  [get]
func (h *handlerV1) GetById(c *gin.Context) {
	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}
	userType, _ := claims["user_type"].(string)
	if userType != "user" {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserTyp, errors.New(models.MessageInvalidUserTyp))
		return
	}
	userId, ok := claims["id"].(string)
	if !ok {
		h.HttpError(c, http.StatusUnauthorized, models.MessageUnauthorized, errors.New("error while getting user_id"))
		return
	}
	res, err := h.strg.User().GetById(context.Background(), repo.IdRequest{
		Id: userId,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorGetUserById, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Delete user
// @Summary User
// @Security ApiKeyAuth
// @Tags user
// @Description This api can delete user by id
// @Accept json
// @Produce json
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/user	[delete]
func (h *handlerV1) DeleteUserById(c *gin.Context) {
	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}
	userType, _ := claims["user_type"].(string)
	if userType != "user" {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserTyp, errors.New(models.MessageInvalidUserTyp))
		return
	}
	userId, ok := claims["id"].(string)
	if !ok {
		h.HttpError(c, http.StatusUnauthorized, models.MessageUnauthorized, errors.New("error while getting user_id"))
		return
	}
	_, err = h.strg.User().DeleteUserById(context.Background(), userId)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorDelUserById, err)
		return
	}

	c.JSON(http.StatusOK, "Succes delete")
}

// Update user
// @Summary User
// @Security ApiKeyAuth
// @Tags user
// @Description This api can update user by fields
// @Accept json
// @Produce json
// @Param data body models.UserReq true "data"
// @Succes 200 {object}  models.User
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/user	[put]
func (h *handlerV1) UpdateUserById(c *gin.Context) {
	var body models.UserReq
	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}
	userType, _ := claims["user_type"].(string)
	if userType != "user" {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserTyp, errors.New(models.MessageInvalidUserTyp))
		return
	}
	userId, ok := claims["id"].(string)
	if !ok {
		h.HttpError(c, http.StatusUnauthorized, models.MessageUnauthorized, errors.New("error while getting user_id"))
		return
	}

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}
	hashedPassword, err := utils.HashPassword(body.Password)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageHashedPassword, err)
		return
	}
	res, err := h.strg.User().UpdateUserById(context.Background(), &repo.User{
		Id:            userId,
		FIO:           body.FIO,
		PhoneNumber:   body.PhoneNumber,
		Gender:        body.Gender,
		Birthday:      body.Birthday,
		Password:      hashedPassword,
		HomeAddress:   body.HomeAddress,
		PassportSerie: body.PassportSerie,
		ImageUpload:   body.ImageUpload,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorUpdateUserById, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Update user
// @Summary User
// @Tags user
// @Description This api can update user by fields
// @Accept json
// @Produce json
// @Param token path string true "token"
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/is_login/{token}	[get]
func (h *handlerV1) IsLogin(c *gin.Context) {
	token := c.Param("token")
	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageInvalidToken, err) {
		return
	}
	Id, _ := claims["id"].(string)

	res, err := h.strg.User().GetById(context.Background(), repo.IdRequest{
		Id: Id,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorUpdateUserById, err)
		return
	}

	c.JSON(http.StatusOK, res)

}
