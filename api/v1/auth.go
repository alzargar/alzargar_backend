package v1

import (
	"alzargar_backend/api/models"
	"alzargar_backend/pkg/utils"
	"alzargar_backend/storage/repo"
	"context"
	"errors"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// Register user
// @Summary Register
// @Tags auth
// @Description This api can register user and create to database
// @Accept json
// @Produce json
// @Param data body models.RegisterReq true "data"
// @Succes 200 {object}  models.RegisterResponse
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/auth/register	[post]
func (h *handlerV1) Register(c *gin.Context) {
	var body models.RegisterReq

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	res, _ := h.strg.User().GetByPhone(context.Background(), body.PhoneNumber)
	if res != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageUserExists, errors.New("user with such a phone number exists"))
		return
	}

	if !IsValidPassword(body.Password) {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidPassword1, errors.New(models.MessageInvalidPassword1))
		return
	}

	hashedPassword, err := utils.HashPassword(body.Password)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageHashedPassword, err)
		return
	}

	if len(body.PhoneNumber) < 9 {
		h.HttpError(c, http.StatusBadRequest, "invalid phone_number", errors.New("invalid phone_number"))
		return
	}
	res, err = h.strg.User().Create(context.Background(), &repo.User{
		FIO:           body.FIO,
		PhoneNumber:   body.PhoneNumber,
		Gender:        body.Gender,
		Birthday:      body.Birthday,
		Password:      hashedPassword,
		HomeAddress:   body.HomeAddress,
		PassportSerie: body.PassportSerie,
		ImageUpload:   body.ImageUpload,
	})
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageErrorCreateToUser, err)
		return
	}

	mp := make(jwt.MapClaims, 0)
	mp["id"] = res.Id
	mp["user_type"] = res.UserType
	accesToken, expiredAt, err := utils.GenerateJWT(mp, h.cfg.AccesTokenExpireDuration, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageGeneratingToken, err) {
		return
	}

	data := models.LoginRes{
		Id:            res.Id,
		FIO:           res.FIO,
		PhoneNumber:   res.PhoneNumber,
		Gender:        res.Gender,
		Birthday:      res.Birthday,
		HomeAddress:   res.HomeAddress,
		PassportSerie: res.PassportSerie,
		ImageUpload:   res.ImageUpload,
		AccesToken:    accesToken,
		ExpiredAt:     expiredAt,
	}

	c.JSON(http.StatusCreated, data)

}

// Login user
// @Summary Login
// @Tags auth
// @Description This api can login user and response acces-token
// @Accept json
// @Produce json
// @Param data body models.LoginReq true "data"
// @Succes 200 {object}  models.LoginRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/auth/login	[post]
func (h *handlerV1) Login(c *gin.Context) {
	var body models.LoginReq

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}
	res, err := h.strg.User().Login(context.Background(), &repo.LoginRequest{
		PhoneNumber: body.PhoneNumber,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidPhoneOrPass, err)
		return
	}
	// checking email and password
	if err := utils.CheckPassword(body.Password, res.Password); err != nil || res.PhoneNumber != body.PhoneNumber {
		h.HttpError(c, http.StatusUnauthorized, models.MessageInvalidPhoneNumberOrPassword, errors.New(models.MessageInvalidPhoneNumberOrPassword))
		return
	}
	mp := make(jwt.MapClaims, 0)

	if body.PhoneNumber == models.AdminPhoneNumber {
		mp["user_type"] = "admin"
	} else {
		mp["id"] = res.Id
		mp["user_type"] = res.UserType
	}

	accesToken, expiredAt, err := utils.GenerateJWT(mp, h.cfg.AccesTokenExpireDuration, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusInternalServerError, models.MessageGeneratingToken, err) {
		return
	}

	data := models.LoginRes{
		Id:            res.Id,
		FIO:           res.FIO,
		PhoneNumber:   res.PhoneNumber,
		Gender:        res.Gender,
		Birthday:      res.Birthday,
		HomeAddress:   res.HomeAddress,
		PassportSerie: res.PassportSerie,
		ImageUpload:   res.ImageUpload,
		AccesToken:    accesToken,
		ExpiredAt:     expiredAt,
	}

	c.JSON(http.StatusOK, data)
}
