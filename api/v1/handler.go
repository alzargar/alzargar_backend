package v1

import (
	"alzargar_backend/api/models"
	"alzargar_backend/config"
	"alzargar_backend/pkg/logger"
	"alzargar_backend/storage"

	"github.com/gin-gonic/gin"
)

type handlerV1 struct {
	cfg  *config.Config
	strg storage.StorageI
	log  logger.Logger
}

type HandlerV1 struct {
	Cfg  *config.Config
	Strg *storage.StorageI
	Log  logger.Logger
}

func New(options *HandlerV1) *handlerV1 {
	return &handlerV1{
		cfg:  options.Cfg,
		strg: *options.Strg,
		log:  options.Log,
	}
}

func (h *handlerV1) HandleHTTPError(c *gin.Context, code int, message string, err error) bool {
	if err != nil {
		h.log.Errorf("Error -> %v", err)
		c.JSON(code, models.FailureResponse{
			Message: message,
			Error:   err.Error(),
		})
		return true
	}
	return false
}

func (h *handlerV1) HttpError(c *gin.Context, code int, message string, err error) {
	h.log.Errorf("Error -> %v", err)
	c.JSON(code, models.FailureResponse{
		Message: message,
		Error:   err.Error(),
	})
}

func IsValidPassword(password string) bool {
	var capitalLetter, smallLetter, number bool
	for i := 0; i < len(password); i++ {
		if password[i] >= 65 && password[i] <= 90 {
			capitalLetter = true
		} else if password[i] >= 97 && password[i] <= 122 {
			smallLetter = true
		} else if password[i] >= 48 && password[i] <= 57 {
			number = true
		}
	}
	return capitalLetter && smallLetter && number
}
