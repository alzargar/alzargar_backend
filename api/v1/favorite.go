package v1

import (
	"alzargar_backend/api/models"
	"alzargar_backend/storage/repo"
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// AddFavoriteProduct
// @Summary AddFavoriteProduct
// @Tags favorite
// @Description This api add favorite product
// @Accept json
// @Produce json
// @Param data body models.FavoriteReq true "data"
// @Succes 200 {object} models.FavoriteRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/favorite [post]
func (h *handlerV1) AddFavoriteProduct(c *gin.Context) {
	var body models.FavoriteReq

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	res, err := h.strg.Favorite().AddFavorite(context.Background(), &repo.FavoriteReq{
		UserID:    body.UserID,
		ProductID: body.ProductID,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, "Failed to add favorite products", err)
		return
	}

	c.JSON(http.StatusCreated, res)
}

// RemoveFavorite
// @Summary RemoveFavorite
// @Tags favorite
// @Description This api remove favorite product
// @Accept json
// @Produce json
// @Param data body models.FavoriteReq true "data"
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/favorite [delete]
func (h *handlerV1) RemoveFavorite(c *gin.Context) {
	var body models.FavoriteReq

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	err := h.strg.Favorite().RemoveFavorite(context.Background(), &repo.FavoriteReq{
		UserID:    body.UserID,
		ProductID: body.ProductID,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, "Failed to add favorite products", err)
		return
	}

	c.JSON(http.StatusOK, "Delete succesfully")
}

// Get favorite products
// @Summary GetFavoruteProducts
// @Tags favorite
// @Description This api get favorite products by user_id
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Succes 200 {object} models.FavoriteRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/favorite/{id}	[get]
func (h *handlerV1) GetFavoruteProducts(c *gin.Context) {
	Id := c.Param("id")

	id, err := strconv.Atoi(Id)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	res, err := h.strg.Favorite().GetFavoriteProducts(context.Background(), id)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, "I thik user_id is wrong", err)
		return
	}

	c.JSON(http.StatusOK, res)
}
