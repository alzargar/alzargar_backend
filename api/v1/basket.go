package v1

import (
	"alzargar_backend/api/models"
	"alzargar_backend/storage/repo"
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Create Basket
// @Summary CreateBasket
// @Description This api create a new basket
// @Tags basket
// @Accept json
// @Produce json
// @Param data body models.BasketReq true "data"
// @Succes 200 {object}  models.BasketRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/basket	[post]
func (h *handlerV1) CreateBasket(c *gin.Context) {
	var body models.BasketReq

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	res, err := h.strg.Basket().CreateBasket(context.Background(), &repo.BasketReq{
		UserId:    body.UserId,
		ProductId: body.ProductId,
		Count:     body.Count,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, "Failed to create basket", err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Get basket by id
// @Summary GetBasketById
// @Tags basket
// @Description This api get basket by id
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Succes 200 {object} models.BasketRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/basket/{id}	[get]
func (h *handlerV1) GetBasketByUserId(c *gin.Context) {
	Id := c.Param("id")

	id, err := strconv.Atoi(Id)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	res, err := h.strg.Basket().GetBasketByUserId(context.Background(), id)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, res)

}

// Update Basket
// @Summary UpdateBasket
// @Description This api update a basket
// @Tags basket
// @Accept json
// @Produce json
// @Param data body models.BasketReqForUpdate true "data"
// @Succes 200 {object}  models.BasketRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/bup	[put]
func (h *handlerV1) UpdateBasket(c *gin.Context) {
	var body models.BasketReqForUpdate

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	res, err := h.strg.Basket().UpdateBasket(context.Background(), &repo.BasketReqForUpdate{
		ID:        body.ID,
		Count:     body.Count,
		ProductId: body.ProductId,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, "Failed to update basket", err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Delete basket by id
// @Summary DeleteBasketById
// @Tags basket
// @Description This api delete
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/bde/{id}	[delete]
func (h *handlerV1) DeleteBasket(c *gin.Context) {
	Id := c.Param("id")

	id, err := strconv.Atoi(Id)
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	err = h.strg.Basket().DeleteBasket(context.Background(), id)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, "Delete Succesfully")

}
