package v1

import (
	"alzargar_backend/api/models"
	"alzargar_backend/pkg/utils"
	"alzargar_backend/storage/repo"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Create Product
// @Summary ProductCreate
// @Security ApiKeyAuth
// @Tags product
// @Description This api create a new product
// @Accept json
// @Produce json
// @Param data body models.ProductReq true "data"
// @Succes 200 {object}  models.ProductRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/product	[post]
func (h *handlerV1) CreateProduct(c *gin.Context) {
	var body models.ProductReq
	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}
	userType, _ := claims["user_type"].(string)
	if userType != "admin" {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserTyp, errors.New(models.MessageInvalidUserTyp))
		return
	}
	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	res, err := h.strg.Product().Create(context.Background(), &repo.ProductReq{
		Category: repo.Category{
			Id: body.Category.Id,
		},
		Title: repo.Language{
			TitleUz:  body.Title.TitleUz,
			TitleEng: body.Title.TitleEng,
			TitleRu:  body.Title.TitleRu,
		},
		IsDiscount:    body.IsDiscount,
		Price:         body.Price,
		DiscountPrice: body.DiscountPrice,
		Rate:          body.Rate,
		Color:         body.Color,
		Shape:         body.Shape,
		Images: repo.Image{

			ImageAbove:    body.Images.ImageAbove,
			ImageFromSide: body.Images.ImageFromSide,
			ImageFromTag:  body.Images.ImageFromTag,
		},
		GenderType: body.GenderType,
		Type:       body.Type,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}
	c.JSON(http.StatusCreated, res)

}

// Get all products
// @Summary ProductsGetAll
// @Tags product
// @Description This api get all products
// @Accept json
// @Produce json
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/product	[get]
func (h *handlerV1) GetAllProducts(c *gin.Context) {

	res, err := h.strg.Product().GetAll(context.Background())
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Get product by category_id
// @Summary ProductsByCategoryId
// @Tags product
// @Description This api get product by category_id
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Succes 200 {object} models.ProductRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/product/{id}	[get]
func (h *handlerV1) GetByCategoryId(c *gin.Context) {
	id := c.Param("id")

	res, err := h.strg.Product().GetById(context.Background(), id)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Get max and min price
// @Summary MinMaxPrice
// @Tags product
// @Description This api get min and max price from product table
// @Accept json
// @Produce json
// @Succes 200 {object} models.MinMaxPrice
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/productmm	[get]
func (h *handlerV1) GetMinMaxPrice(c *gin.Context) {
	res, err := h.strg.Product().GetMinMaxPrice(context.Background())
	if err != nil {
		h.HttpError(c, http.StatusInternalServerError, models.MessageInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Get max and min price
// @Summary MinMaxPrice
// @Tags product
// @Description This api get min and max price from product table
// @Accept json
// @Produce json
// @Param min path string true "MinPrice"
// @Param max path string true "MaxPrice"
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/products/{min}/{max}	[get]
func (h *handlerV1) GeByPrice(c *gin.Context) {
	min := c.Param("min")
	max := c.Param("max")

	minP, err := strconv.ParseFloat(min, 64)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorParseFloat, err)
		return
	}

	maxP, err := strconv.ParseFloat(max, 64)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorParseFloat, err)
		return
	}

	res, err := h.strg.Product().GetPrice(context.Background(), &repo.MinMaxPrice{
		MinPrice: minP,
		MaxPrice: maxP,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Search products
// @Summary SearchProduct
// @Tags product
// @Description This api search products
// @Accept json
// @Produce json
// @Param language path string true "language"
// @Param search path string true "search"
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/search/{language}/{search}	[get]
func (h *handlerV1) SearchProduct(c *gin.Context) {
	language := c.Param("language")
	search := c.Param("search")

	if language == "uz" {
		resUz, err := h.strg.Product().SearchUz(context.Background(), &repo.Search{
			Search: search,
		})
		if err != nil {
			h.HttpError(c, http.StatusBadRequest, models.MessageInvalidLanguage, err)
			return
		}
		c.JSON(http.StatusOK, resUz)
	} else if language == "ru" {
		resRu, err := h.strg.Product().SearchRu(context.Background(), &repo.Search{
			Search: search,
		})
		if err != nil {
			h.HttpError(c, http.StatusBadRequest, models.MessageInvalidLanguage, err)
			return
		}
		c.JSON(http.StatusOK, resRu)

	} else if language == "en" {
		resEn, err := h.strg.Product().SearchEn(context.Background(), &repo.Search{
			Search: search,
		})
		if err != nil {
			h.HttpError(c, http.StatusBadRequest, models.MessageInvalidLanguage, err)
			return
		}
		c.JSON(http.StatusOK, resEn)

	} else {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidLanguage, errors.New(models.MessageInvalidLanguage))
		return

	}

}

// Delete product by id
// @Summary ProductDeleteById
// @Security ApiKeyAuth
// @Tags product
// @Description This api delete product by id
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Succes 200 {object} models.ProductRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/product/{id}		[delete]
func (h *handlerV1) DeleteProductById(c *gin.Context) {
	Id := c.Param("id")
	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}
	userType, _ := claims["user_type"].(string)
	if userType != "admin" {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserTyp, errors.New(models.MessageInvalidUserTyp))
		return
	}
	err = h.strg.Product().DeleteProduct(context.Background(), Id)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, "Delete Succesfully")
}

// Update Product
// @Summary ProductUpdate
// @Security ApiKeyAuth
// @Tags product
// @Description This api update product
// @Accept json
// @Produce json
// @Param data body models.ReqForUpdate true "data"
// @Succes 200 {object}  models.ProductRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/product	[put]
func (h *handlerV1) UpdateProduct(c *gin.Context) {
	var body models.ReqForUpdate
	var token = c.Request.Header.Get("Authorization")

	claims, err := utils.ExtractClaims(token, h.cfg.TokenSecretKey)
	if h.HandleHTTPError(c, http.StatusUnauthorized, models.MessageUnauthorized, err) {
		return
	}
	userType, _ := claims["user_type"].(string)
	if userType != "admin" {
		h.HttpError(c, http.StatusBadRequest, models.MessageInvalidUserTyp, errors.New(models.MessageInvalidUserTyp))
		return
	}

	if err := c.ShouldBindJSON(&body); h.HandleHTTPError(c, http.StatusBadRequest, models.MessageShouldBindJSON, err) {
		return
	}

	res, err := h.strg.Product().UpdateProduct(context.Background(), &repo.ReqForUpdate{
		Id: body.Id,
		Category: repo.Category{
			Id: body.Category.Id,
		},
		Title: repo.Language{
			TitleUz:  body.Title.TitleUz,
			TitleEng: body.Title.TitleEng,
			TitleRu:  body.Title.TitleRu,
		},
		IsDiscount:    body.IsDiscount,
		Price:         body.Price,
		DiscountPrice: body.DiscountPrice,
		Rate:          body.Rate,
		Color:         body.Color,
		Shape:         body.Shape,
		Images: repo.Image{
			ImageAbove:    body.Images.ImageAbove,
			ImageFromSide: body.Images.ImageFromSide,
			ImageFromTag:  body.Images.ImageFromTag,
		},
		GenderType: body.GenderType,
		Type:       body.Type,
	})
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return

	}
	c.JSON(http.StatusCreated, res)

}
