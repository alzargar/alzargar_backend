package v1

import (
	"alzargar_backend/api/models"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Get category by id
// @Summary GetCategoryById
// @Tags category
// @Description This api get category by id
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Succes 200 {object} models.CategoryRes
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/{id}		[get]
func (h *handlerV1) GetCategoryById(c *gin.Context) {
	Id := c.Param("id")

	res, err := h.strg.Category().GetById(context.Background(), Id)
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, res)
}

// Get All Categories
// @Summary GetAllCategory
// @Tags category
// @Description This api get all category
// @Accept json
// @Produce json
// @Succes 200 {object}
// @Failure 400 {object} models.FailureResponse
// @Failure 401 {object} models.FailureResponse
// @Failure 404 {object} models.FailureResponse
// @Failure 500 {object} models.FailureResponse
// @Router /v1/category		[get]
func (h *handlerV1) GetAllCategory(c *gin.Context) {

	res, err := h.strg.Category().GetAllCategories(context.Background())
	if err != nil {
		h.HttpError(c, http.StatusBadRequest, models.MessageErrorSmthng, err)
		return
	}

	c.JSON(http.StatusOK, res)
}
