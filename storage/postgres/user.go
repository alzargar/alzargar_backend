package postgres

import (
	"alzargar_backend/storage/repo"
	"context"
	"database/sql"
	"log"
	"time"
)

type userRepo struct {
	db *sql.DB
}

func NewUserRepo(db *sql.DB) *userRepo {
	return &userRepo{db: db}
}

func (r *userRepo) Create(ctx context.Context, user *repo.User) (*repo.User, error) {
	var resp repo.User

	err := r.db.QueryRow("INSERT INTO users(fio, phone_number, gender, birthday, password, home_address, passport_series, image_upload, user_type) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id,fio, phone_number, gender, birthday, password, home_address, passport_series, image_upload, user_type", user.FIO, user.PhoneNumber, user.Gender, user.Birthday, user.Password, user.HomeAddress, user.PassportSerie, user.ImageUpload, "user").Scan(
		&resp.Id,
		&resp.FIO,
		&resp.PhoneNumber,
		&resp.Gender,
		&resp.Birthday,
		&resp.Password,
		&resp.HomeAddress,
		&resp.PassportSerie,
		&resp.ImageUpload,
		&resp.UserType,
	)

	if err != nil {
		log.Println("Failed to create user", err)
	}

	return &resp, nil

}

func (r *userRepo) Login(ctx context.Context, req *repo.LoginRequest) (*repo.LoginResponse, error) {
	var res repo.LoginResponse
	err := r.db.QueryRow("SELECT id, fio, phone_number, gender, birthday, password, home_address, passport_series, image_upload, user_type FROM users WHERE phone_number=$1 AND deleted_at IS NULL", req.PhoneNumber).Scan(
		&res.Id,
		&res.FIO,
		&res.PhoneNumber,
		&res.Gender,
		&res.Birthday,
		&res.Password,
		&res.HomeAddress,
		&res.PassportSerie,
		&res.ImageUpload,
		&res.UserType,
	)
	if err != nil {
		log.Println("Failed to select user info from login func in psql: ", err)
	}

	return &res, nil
}

func (r *userRepo) GetByPhone(ctx context.Context, reqPhone string) (*repo.User, error) {
	var res repo.User
	err := r.db.QueryRow("SELECT id, fio, phone_number, gender, birthday, password, home_address, passport_series, image_upload, user_type FROM users WHERE phone_number=$1 AND deleted_at IS NULL", reqPhone).Scan(
		&res.Id,
		&res.FIO,
		&res.PhoneNumber,
		&res.Gender,
		&res.Birthday,
		&res.Password,
		&res.HomeAddress,
		&res.PassportSerie,
		&res.ImageUpload,
		&res.UserType,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (r *userRepo) GetById(ctx context.Context, user repo.IdRequest) (*repo.User, error) {
	var res repo.User

	err := r.db.QueryRow("SELECT id, fio, phone_number, gender, birthday, password, home_address, passport_series, image_upload, user_type FROM users WHERE id=$1 AND deleted_at IS NULL", user.Id).Scan(
		&res.Id,
		&res.FIO,
		&res.PhoneNumber,
		&res.Gender,
		&res.Birthday,
		&res.Password,
		&res.HomeAddress,
		&res.PassportSerie,
		&res.ImageUpload,
		&res.UserType,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (r *userRepo) UpdateUserById(ctx context.Context, user *repo.User) (*repo.User, error) {
	var res repo.User

	err := r.db.QueryRow(`UPDATE users 
				SET 
				fio=$1, 
				phone_number=$2,
				gender=$3,
				birthday=$4,
				password=$5, 
				home_address=$6, 
				passport_series=$7,
				image_upload=$8
					WHERE id=$9 RETURNING id, fio, phone_number, gender, birthday, password, home_address, passport_series, image_upload,user_type`,
		user.FIO,
		user.PhoneNumber,
		user.Gender,
		user.Birthday,
		user.Password,
		user.HomeAddress,
		user.PassportSerie,
		user.ImageUpload,
		user.Id,
	).Scan(
		&res.Id,
		&res.FIO,
		&res.PhoneNumber,
		&res.Gender,
		&res.Birthday,
		&res.Password,
		&res.HomeAddress,
		&res.PassportSerie,
		&res.ImageUpload,
		&res.UserType,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (r *userRepo) DeleteUserById(ctx context.Context, Id string) (*repo.User, error) {
	_, err := r.db.Exec("UPDATE users SET deleted_at=$1 WHERE id = $2 AND deleted_at IS NULL", time.Now(), Id)
	if err != nil {
		return nil, err
	}

	return &repo.User{}, err

}
