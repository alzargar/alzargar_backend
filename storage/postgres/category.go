package postgres

import (
	"alzargar_backend/storage/repo"
	"context"
	"database/sql"
	"encoding/json"
	"log"
)

type categoryRepo struct {
	db *sql.DB
}

func NewCategoryRepo(db *sql.DB) *categoryRepo {
	return &categoryRepo{db: db}
}

func (r *categoryRepo) GetById(ctx context.Context, Id string) (*repo.CategoryRes, error) {
	var res repo.CategoryRes

	var data []byte

	err := r.db.QueryRow("SELECT id,name_uz, name_ru, name_en, image_url, link, data, created_at, updated_at FROM category WHERE id=$1", Id).Scan(
		&res.Id,
		&res.Name.Uz,
		&res.Name.Ru,
		&res.Name.En,
		&res.Image_url,
		&res.Link,
		&data,
		&res.CreatedAt,
		&res.UpdatedAt,
	)
	if err != nil {
		log.Println("Failed to scan category: ", err)
	}
	if data == nil {
		// Handle the null case
		res.Data = []repo.Data{}
	} else {
		err = json.Unmarshal(data, &res.Data)
		if err != nil {
			log.Println("Failed to unmarshal data from JSON:", err)
			return nil, err
		}
	}

	return &res, nil

}

func (r *categoryRepo) GetAllCategories(ctx context.Context) (*repo.AllCategory, error) {
	var resp repo.AllCategory

	var data []byte

	rows, err := r.db.Query("SELECT id,name_uz, name_ru, name_en, image_url, link, data, created_at, updated_at FROM category")
	if err != nil {
		log.Println("Failed to scan category: ", err)
	}
	for rows.Next() {
		var res repo.CategoryRes
		err := rows.Scan(
			&res.Id,
			&res.Name.Uz,
			&res.Name.Ru,
			&res.Name.En,
			&res.Image_url,
			&res.Link,
			&data,
			&res.CreatedAt,
			&res.UpdatedAt,
		)
		if err != nil {
			log.Println("Failed to scan all categories", err)
		}

		if data == nil {
			// Handle the null case
			res.Data = []repo.Data{}
		} else {
			err = json.Unmarshal(data, &res.Data)
			if err != nil {
				log.Println("Failed to unmarshal data from JSON:", err)
				return nil, err
			}
		}
		resp.Categories = append(resp.Categories, res)
	}
	return &resp, nil

}
