package postgres

import (
	"alzargar_backend/storage/repo"
	"context"
	"database/sql"
	"log"
)

type basketRepo struct {
	db *sql.DB
}

func NewBasketRepo(db *sql.DB) *basketRepo {
	return &basketRepo{db: db}
}

func (r *basketRepo) CreateBasket(ctx context.Context, req *repo.BasketReq) (*repo.BasketRes, error) {
	var res repo.BasketRes

	err := r.db.QueryRow(`INSERT INTO 
								basket(
									user_id,
									product_id,
									counts
								) VALUES($1,$2,$3)
									RETURNING
									id,
									user_id,
									product_id,
									counts,
									created_at
	`, req.UserId, req.ProductId, req.Count).Scan(
		&res.ID,
		&res.UserId,
		&res.ProductId,
		&res.Count,
		&res.CreatedAt,
	)
	if err != nil {
		log.Println("Failed to create basket", err)
	}

	return &res, nil

}

func (r *basketRepo) GetBasketByUserId(ctx context.Context, UserId int) (*repo.BasketRes, error) {
	var res repo.BasketRes

	err := r.db.QueryRow("SELECT id,user_id, product_id, counts, created_at FROM basket WHERE user_id=$1", UserId).Scan(
		&res.ID,
		&res.UserId,
		&res.ProductId,
		&res.Count,
		&res.CreatedAt,
	)
	if err != nil {
		log.Println("Failed to get basket by id: ", err)
	}

	return &res, nil
}

func (r *basketRepo) UpdateBasket(ctx context.Context, req *repo.BasketReqForUpdate) (*repo.BasketRes, error) {
	var res repo.BasketRes

	err := r.db.QueryRow("UPDATE basket SET counts=$1 WHERE id=$2 AND product_id=$3 RETURNING id, user_id, product_id, counts, created_at", req.Count, req.ID, req.ProductId).Scan(
		&res.ID,
		&res.UserId,
		&res.ProductId,
		&res.Count,
		&res.CreatedAt,
	)
	if err != nil {
		log.Println("Failed to update basket: ", err)
	}

	return &res, nil
}

func (r *basketRepo) DeleteBasket(ctx context.Context, Id int) error {
	_, err := r.db.Exec("DELETE FROM basket WHERE id=$1", Id)
	if err != nil {
		log.Println("Failed to delete basket: ", err)
	}

	return nil
}
