package postgres

import (
	"alzargar_backend/storage/repo"
	"context"
	"database/sql"
	"log"
)

type favoriteRepo struct {
	db *sql.DB
}

func NewFavoriteRepo(db *sql.DB) *favoriteRepo {
	return &favoriteRepo{db: db}
}

func (r *favoriteRepo) AddFavorite(ctx context.Context, favorite *repo.FavoriteReq) (*repo.FavoriteRes, error) {
	var res repo.FavoriteRes

	err := r.db.QueryRow(`INSERT INTO favorites(user_id, product_id) VALUES($1, $2) RETURNING user_id, product_id`, favorite.UserID, favorite.ProductID).Scan(
		&res.UserID,
		&res.ProductID,
	)
	if err != nil {
		log.Println("Failed to create favorite product: ", err)
	}

	return &res, err
}

func (r *favoriteRepo) RemoveFavorite(ctx context.Context, favorite *repo.FavoriteReq) error {
	err := r.db.QueryRow("DELETE FROM favorites WHERE user_id=$1 AND product_id=$2", favorite.UserID, favorite.ProductID)

	if err != nil {
		log.Println("Failed to remove product", err)
	}

	return nil
}

func (r *favoriteRepo) GetFavoriteProducts(ctx context.Context, userId int) ([]int, error) {
	var resp []int

	rows, err := r.db.Query("SELECT product_id FROM favorites WHERE user_id=$1", userId)
	if err != nil {
		log.Println("Failed to get favorites products: ", err)
	}

	for rows.Next() {
		var productID int
		err := rows.Scan(&productID)
		if err != nil {
			log.Println("Failed to scan favorite products: ", err)
		}
		resp = append(resp, productID)
	}

	return resp, nil
}
