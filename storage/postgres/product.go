package postgres

import (
	"alzargar_backend/storage/repo"
	"context"
	"database/sql"
	"log"
)

type productRepo struct {
	db *sql.DB
}

func NewProductRepo(db *sql.DB) *productRepo {
	return &productRepo{db: db}
}

func (r *productRepo) Create(ctx context.Context, req *repo.ProductReq) (*repo.ProductRes, error) {
	var resp repo.ProductRes

	err := r.db.QueryRow(`INSERT INTO 
					products(category_id,
							name_uz,
							name_eng,
							name_ru,
							is_discount,
							price,
							discount_price,
							rate,
							color,
							shape,
							image_above,
							image_side,
							image_tag,
							gender_type,
							type)
						VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)
							RETURNING
							id,
							category_id,
							name_uz,
							name_eng,
							name_ru,
							is_discount,
							price,
							discount_price,
							rate,
							color,
							shape,
							image_above,
							image_side,
							image_tag,
							gender_type,
							type,
							created_at`,
		req.Category.Id,
		req.Title.TitleUz,
		req.Title.TitleEng,
		req.Title.TitleRu,
		req.IsDiscount,
		req.Price,
		req.DiscountPrice,
		req.Rate,
		req.Color,
		req.Shape,
		req.Images.ImageAbove,
		req.Images.ImageFromSide,
		req.Images.ImageFromTag,
		req.GenderType,
		req.Type,
	).Scan(
		&resp.Id,
		&resp.Category.Id,
		&resp.Title.TitleUz,
		&resp.Title.TitleEng,
		&resp.Title.TitleRu,
		&resp.IsDiscount,
		&resp.Price,
		&resp.DiscountPrice,
		&resp.Rate,
		&resp.Color,
		&resp.Shape,
		&resp.Images.ImageAbove,
		&resp.Images.ImageFromSide,
		&resp.Images.ImageFromTag,
		&resp.GenderType,
		&resp.Type,
		&resp.CreatedAt,
	)
	if err != nil {
		log.Println("Failed to create product", err)
	}

	return &resp, nil

}

func (r *productRepo) GetAll(ctx context.Context) (*repo.AllProducts, error) {
	var resp repo.AllProducts

	rows, err := r.db.Query(`SELECT id,
							category_id,
							name_uz,
							name_eng,
							name_ru,
							is_discount,
							price,
							discount_price,
							rate,
							color,
							shape,
							image_above,
							image_side,
							image_tag,
							gender_type,
							type,
							created_at
					FROM products
							`)
	if err != nil {
		log.Println("Failed to get all products", err)
	}

	for rows.Next() {
		var res repo.ProductRes
		err := rows.Scan(
			&res.Id,
			&res.Category.Id,
			&res.Title.TitleUz,
			&res.Title.TitleEng,
			&res.Title.TitleRu,
			&res.IsDiscount,
			&res.Price,
			&res.DiscountPrice,
			&res.Rate,
			&res.Color,
			&res.Shape,
			&res.Images.ImageAbove,
			&res.Images.ImageFromSide,
			&res.Images.ImageFromTag,
			&res.GenderType,
			&res.Type,
			&res.CreatedAt,
		)
		if err != nil {
			log.Println("Failed to scan all products", err)
		}
		resp.Products = append(resp.Products, res)
	}

	return &resp, nil
}

func (r *productRepo) GetById(ctx context.Context, Id string) (*repo.AllProducts, error) {
	var resp repo.AllProducts

	rows, err := r.db.Query(`SELECT id,
							category_id,
							name_uz,
							name_eng,
							name_ru,
							is_discount,
							price,
							discount_price,
							rate,
							color,
							shape,
							image_above,
							image_side,
							image_tag,
							gender_type,
							type,
							created_at
					FROM products WHERE category_id=$1
							`, Id)
	if err != nil {
		log.Println("Failed to get all products", err)
	}

	for rows.Next() {
		var res repo.ProductRes
		err := rows.Scan(
			&res.Id,
			&res.Category.Id,
			&res.Title.TitleUz,
			&res.Title.TitleEng,
			&res.Title.TitleRu,
			&res.IsDiscount,
			&res.Price,
			&res.DiscountPrice,
			&res.Rate,
			&res.Color,
			&res.Shape,
			&res.Images.ImageAbove,
			&res.Images.ImageFromSide,
			&res.Images.ImageFromTag,
			&res.GenderType,
			&res.Type,
			&res.CreatedAt,
		)
		if err != nil {
			log.Println("Failed to scan all products", err)
		}
		resp.Products = append(resp.Products, res)
	}

	return &resp, nil
}

func (r *productRepo) GetMinMaxPrice(ctx context.Context) (*repo.MinMaxPrice, error) {
	var res repo.MinMaxPrice

	err := r.db.QueryRow("SELECT MIN(price), MAX(price) FROM products").Scan(
		&res.MinPrice,
		&res.MaxPrice,
	)
	if err != nil {
		log.Println("Failed to get min and max price", err)
	}

	return &res, nil

}

func (r *productRepo) GetPrice(ctx context.Context, req *repo.MinMaxPrice) (*repo.AllProducts, error) {
	var resp repo.AllProducts

	rows, err := r.db.Query(`SELECT id,
							category_id,
							name_uz,
							name_eng,
							name_ru,
							is_discount,
							price,
							discount_price,
							rate,
							color,
							shape,
							image_above,
							image_side,
							image_tag,
							gender_type,
							type,
							created_at
						FROM products WHERE price BETWEEN $1 AND $2
	`, req.MinPrice, req.MaxPrice)
	if err != nil {
		log.Println("Failed to get products with min and max price", err)
	}

	for rows.Next() {
		var res repo.ProductRes
		err := rows.Scan(
			&res.Id,
			&res.Category.Id,
			&res.Title.TitleUz,
			&res.Title.TitleEng,
			&res.Title.TitleRu,
			&res.IsDiscount,
			&res.Price,
			&res.DiscountPrice,
			&res.Rate,
			&res.Color,
			&res.Shape,
			&res.Images.ImageAbove,
			&res.Images.ImageFromSide,
			&res.Images.ImageFromTag,
			&res.GenderType,
			&res.Type,
			&res.CreatedAt,
		)
		if err != nil {
			log.Println("Failed to scan products with min and max price", err)
		}

		resp.Products = append(resp.Products, res)
	}

	return &resp, nil
}

func (r *productRepo) SearchUz(ctx context.Context, req *repo.Search) (*repo.AllProductsUz, error) {
	var resp repo.AllProductsUz

	rows, err := r.db.Query("SELECT id,category_id,name_uz,is_discount,price,discount_price,rate,color,shape,image_above,image_side,image_tag,gender_type,type,created_at FROM products WHERE name_uz ILIKE '%" + req.Search + "%'")
	if err != nil {
		log.Println("Failed to search user in uzbek language", err)
	}

	for rows.Next() {
		var res repo.ProductResUz
		err := rows.Scan(
			&res.Id,
			&res.Category.Id,
			&res.TitleUz,
			&res.IsDiscount,
			&res.Price,
			&res.DiscountPrice,
			&res.Rate,
			&res.Color,
			&res.Shape,
			&res.Images.ImageAbove,
			&res.Images.ImageFromSide,
			&res.Images.ImageFromTag,
			&res.GenderType,
			&res.Type,
			&res.CreatedAt,
		)
		if err != nil {
			log.Println("Failed to scan user in uzbek language", err)
		}

		resp.Products = append(resp.Products, res)
	}

	return &resp, nil
}

func (r *productRepo) SearchRu(ctx context.Context, req *repo.Search) (*repo.AllProductsRu, error) {
	var resp repo.AllProductsRu

	rows, err := r.db.Query("SELECT id,category_id,name_ru,is_discount,price,discount_price,rate,color,shape,image_above,image_side,image_tag,gender_type,type,created_at FROM products WHERE name_ru ILIKE '%" + req.Search + "%'")
	if err != nil {
		log.Println("Failed to search user in russian language", err)
	}

	for rows.Next() {
		var res repo.ProductResRu
		err := rows.Scan(
			&res.Id,
			&res.Category.Id,
			&res.TitleRu,
			&res.IsDiscount,
			&res.Price,
			&res.DiscountPrice,
			&res.Rate,
			&res.Color,
			&res.Shape,
			&res.Images.ImageAbove,
			&res.Images.ImageFromSide,
			&res.Images.ImageFromTag,
			&res.GenderType,
			&res.Type,
			&res.CreatedAt,
		)
		if err != nil {
			log.Println("Failed to scan user in russian language", err)
		}

		resp.Products = append(resp.Products, res)
	}

	return &resp, nil
}

func (r *productRepo) SearchEn(ctx context.Context, req *repo.Search) (*repo.AllProductsEn, error) {
	var resp repo.AllProductsEn

	rows, err := r.db.Query("SELECT id,category_id,name_eng,is_discount,price,discount_price,rate,color,shape,image_above,image_side,image_tag,gender_type,type,created_at FROM products WHERE name_eng ILIKE '%" + req.Search + "%'")
	if err != nil {
		log.Println("Failed to search user in english language", err)
	}

	for rows.Next() {
		var res repo.ProductResEn
		err := rows.Scan(
			&res.Id,
			&res.Category.Id,
			&res.TitleEn,
			&res.IsDiscount,
			&res.Price,
			&res.DiscountPrice,
			&res.Rate,
			&res.Color,
			&res.Shape,
			&res.Images.ImageAbove,
			&res.Images.ImageFromSide,
			&res.Images.ImageFromTag,
			&res.GenderType,
			&res.Type,
			&res.CreatedAt,
		)
		if err != nil {
			log.Println("Failed to scan user in english language", err)
		}

		resp.Products = append(resp.Products, res)
	}

	return &resp, nil
}



func (r *productRepo) UpdateProduct(ctx context.Context, req *repo.ReqForUpdate) (*repo.ProductRes, error) {
	var res repo.ProductRes

	err := r.db.QueryRow(`UPDATE products 
							SET  
										category_id=$1,
										name_uz=$2,
										name_eng=$3,
										name_ru=$4,
										is_discount=$5,
										price=$6,
										discount_price=$7,
										rate=$8,
										color=$9,
										shape=$10,
										image_above=$11,
										image_side=$12,
										image_tag=$13,
										gender_type=$14,
										type=$15
										WHERE id=$16
							RETURNING 
										id,
										category_id,
										name_uz,
										name_eng,
										name_ru,
										is_discount,
										price,
										discount_price,
										rate,
										color,
										shape,
										image_above,
										image_side,
										image_tag,
										gender_type,
										type,
										updated_at`,
		req.Category.Id,
		req.Title.TitleUz,
		req.Title.TitleEng,
		req.Title.TitleRu,
		req.IsDiscount,
		req.Price,
		req.DiscountPrice,
		req.Rate,
		req.Color,
		req.Shape,
		req.Images.ImageAbove,
		req.Images.ImageFromSide,
		req.Images.ImageFromTag,
		req.GenderType,
		req.Type,
		req.Id,
	).Scan(
		&res.Id,
		&res.Category.Id,
		&res.Title.TitleUz,
		&res.Title.TitleEng,
		&res.Title.TitleRu,
		&res.IsDiscount,
		&res.Price,
		&res.DiscountPrice,
		&res.Rate,
		&res.Color,
		&res.Shape,
		&res.Images.ImageAbove,
		&res.Images.ImageFromSide,
		&res.Images.ImageFromTag,
		&res.GenderType,
		&res.Type,
		&res.UpdatedAt,
	)
	if err != nil {
		log.Println("Failed to update product", err)
	}

	return &res, nil

}

func (r *productRepo) DeleteProduct(ctx context.Context, Id string) error {

	err := r.db.QueryRow("DELETE FROM products WHERE id=$1", Id)
	if err != nil {
		log.Println("Failed to delete product", err)
	}

	return nil
}
