package storage

import (
	"alzargar_backend/storage/postgres"
	"alzargar_backend/storage/repo"
	"database/sql"
)

type StorageI interface {
	User() repo.UserI
	Category() repo.CategoryI
	Product() repo.ProductI
	Basket() repo.BasketI
	Favorite() repo.FavoritesI
}

type storagePG struct {
	db           *sql.DB
	userRepo     repo.UserI
	categoryRepo repo.CategoryI
	productRepo  repo.ProductI
	basketRepo   repo.BasketI
	favoriteRepo repo.FavoritesI
}

func NewStoragePg(db *sql.DB) *storagePG {
	return &storagePG{
		db:           db,
		userRepo:     postgres.NewUserRepo(db),
		categoryRepo: postgres.NewCategoryRepo(db),
		productRepo:  postgres.NewProductRepo(db),
		basketRepo:   postgres.NewBasketRepo(db),
		favoriteRepo: postgres.NewFavoriteRepo(db),
	}
}

func (s storagePG) User() repo.UserI {
	return s.userRepo
}

func (s storagePG) Category() repo.CategoryI {
	return s.categoryRepo
}

func (s storagePG) Product() repo.ProductI {
	return s.productRepo
}

func (s storagePG) Basket() repo.BasketI {
	return s.basketRepo
}

func (s storagePG) Favorite() repo.FavoritesI {
	return s.favoriteRepo
}
