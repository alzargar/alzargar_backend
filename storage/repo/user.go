package repo

import (
	"context"
)

type UserI interface {
	Create(ctx context.Context, user *User) (*User, error)
	Login(ctx context.Context, req *LoginRequest) (*LoginResponse, error)
	GetByPhone(ctx context.Context, Phone string) (*User, error)
	GetById(ctx context.Context, Id IdRequest) (*User, error)
	UpdateUserById(ctx context.Context, user *User) (*User, error)
	DeleteUserById(ctx context.Context, Id string) (*User, error)
}

type IdRequest struct {
	Id string
}

type User struct {
	Id            string `json:"id" binding:"required"`
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	Password      string `json:"password" binding:"required"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
	UserType      string `json:"user_type"`
}

type LoginRequest struct {
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
}

type LoginResponse struct {
	Id            string `json:"id" binding:"required"`
	FIO           string `json:"fio" binding:"required"`
	PhoneNumber   string `json:"phone_number"`
	Gender        string `json:"gender"`
	Birthday      string `json:"birthday"`
	Password      string `json:"password" binding:"required"`
	HomeAddress   string `json:"home_address"`
	PassportSerie string `json:"passport_series"`
	ImageUpload   string `json:"image_upload"`
	UserType      string `json:"user_type"`
}
