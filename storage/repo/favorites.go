package repo

import "context"

type FavoritesI interface {
	AddFavorite(ctx context.Context, req *FavoriteReq) (*FavoriteRes, error)
	RemoveFavorite(ctx context.Context, req *FavoriteReq) error
	GetFavoriteProducts(ctx context.Context, userID int) ([]int, error)
}

type FavoriteReq struct {
	UserID    int `json:"user_id"`
	ProductID int `json:"product_id"`
}

type FavoriteRes struct {
	UserID    int `json:"user_id"`
	ProductID int `json:"product_id"`
}


