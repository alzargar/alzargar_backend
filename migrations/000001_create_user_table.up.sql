CREATE TABLE IF NOT EXISTS users(
    id SERIAL PRIMARY KEY,
    fio TEXT,
    phone_number TEXT UNIQUE,
    gender TEXT,
    birthday TEXT, 
    password TEXT UNIQUE,
    home_address TEXT,
    passport_series TEXT UNIQUE,
    image_upload TEXT,
    user_type VARCHAR(20),
    created_at TIME DEFAULT CURRENT_TIMESTAMP,
    updated_at TIME DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIME
);  

-- PASSWORD IS Isma02
INSERT INTO users(fio, phone_number, gender, birthday, password, home_address, passport_series, user_type) VALUES ('It is ForTest', '938108406', 'male', '02-12-2000', '$2a$10$qS1.LMvts92D8iADmU1.leC5K6Ncs6h4/Zl2wKSJf5/TxNXkPPfwe', 'New York City', 'AA123456', 'admin');