CREATE TABLE IF NOT EXISTS category(
    id SERIAL PRIMARY KEY,
    "name_uz" TEXT,
    "name_ru" TEXT,
    "name_en" TEXT,
    image_url TEXT,
    link      TEXT,
    "data"    JSONB,
    created_at TIME DEFAULT CURRENT_TIMESTAMP,
    updated_at TIME DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIME
);

INSERT INTO category(name_uz, name_ru, name_en, image_url, link,data) VALUES('uzuklar', 'rings', 'кольца', 'http://alzargar.uz/media/3b1dad2f-1fa7-4787-bc50-a02688ad9956.jpg', 'rings/all', '[{"link" : "wedding", "name" : {"uz" : "nikoh uzuklari", "en" : "wedding rings", "ru" : "обручальные кольца"}}, {"link" : "stones", "name" : {"uz" : "Qimmatbaho toshlar bilan", "en" : "With precious stones", "ru" : "С драгоценными камнями"}}]');



INSERT INTO category(name_uz, name_ru, name_en, image_url, link) VALUES('Bilakuzuklar', 'Bracelets', 'Браслеты', 'http://alzargar.uz/media/d9ed2c30-d762-41cf-8841-50d0d507bd2c.jpg', 'bracelet');


INSERT INTO category(name_uz, name_ru, name_en, image_url, link) VALUES('Marjon va Zanjirlar', 'Necklaces and Chains', 'Ожерелья и цепочки', 'http://alzargar.uz/media/64fbf205-6599-423c-940d-231413f2cd62.jpg', 'chains');

INSERT INTO category(name_uz, name_ru, name_en, image_url, link) VALUES('Toplamlar', 'Totals', 'Итоги', 'http://alzargar.uz/media/c8de0cc6-10fc-4c24-bac9-c3f079687536.jpg', 'collections');

INSERT INTO category(name_uz, name_ru, name_en, image_url, link) VALUES('Sirgalar', 'Earrings', 'Серьги', 'http://alzargar.uz/media/3178582f-5760-4213-9690-480f8c6abe80.jpg', 'earrings');
