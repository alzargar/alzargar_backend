CREATE TABLE IF NOT EXISTS basket(
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users(id),
    product_id INT REFERENCES products(id),
    counts INT,
    created_at TIME DEFAULT CURRENT_TIMESTAMP
);