package config

import (
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	AccesTokenExpireDuration time.Duration
	TokenSecretKey           string
	Postgres                 Postgres
	HttpPort                 string
}

type Postgres struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		AccesTokenExpireDuration: conf.GetDuration("ACCESS_TOKEN_DURATION"),
		TokenSecretKey:           conf.GetString("TOKEN_SECRET_KEY"),
		HttpPort:                 conf.GetString("HTTP_PORT"),
		Postgres: Postgres{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},
	}

	return cfg
}
