package main

import (
	"alzargar_backend/api"
	"alzargar_backend/config"
	"alzargar_backend/pkg/db"
	"alzargar_backend/pkg/logger"
	"alzargar_backend/storage"
)

func main() {
	logger.Init()
	log := logger.GetLogger()

	// intiliazing config
	cfg := config.Load(".")

	connDb, err := db.ConnectDB(cfg)
	if err != nil {
		log.Fatal("Failed to connection database", err)
	}

	strg := storage.NewStoragePg(connDb)

	apiServer := api.New(&api.RouterOptions{
		Cfg:     &cfg,
		Storage: strg,
		Log:     log,
	})

	if err = apiServer.Run(cfg.HttpPort); err != nil {
		log.Errorf("failed to run server: %s", err)
	}
}
