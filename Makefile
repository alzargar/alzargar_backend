.SILENT:
.PHONY:

run:
	go run cmd/main.go

tidy:
	go mod tidy
	go mod vendor

swag-init:
	swag init -g api/api.go -o api/docs

test:
	go test --short -coverprofile=cover.out -v ./...
	make test.coverage

test.coverage:
	go tool cover -func=cover.out | grep "total"

lint:
	golangci-lint run
	
migrate-create:
	migrate create -ext sql -dir ./migrations -seq create_favorites_table
 
migrate-path:
	migrate -path migrations/ -database postgres://ismoiljon12:12@localhost:5432/alzargar_db up