package utils

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

// GenerateJWT function is to generate token based on given MapClaims
func GenerateJWT(mapClaims jwt.MapClaims, tokenExpireTime time.Duration, tokenSecretKey string) (tokenString string, expiresAt int64, err error) {
	var (
		token = jwt.New(jwt.SigningMethodHS256)
	)

	claims := token.Claims.(jwt.MapClaims)

	for key, value := range mapClaims {
		claims[key] = value
	}

	claims["iat"] = time.Now().Unix()

	expiresAt = time.Now().Add(tokenExpireTime).Unix()
	claims["exp"] = expiresAt

	tokenString, err = token.SignedString([]byte(tokenSecretKey))
	if err != nil {
		return "", 0, errors.Wrap(err, " invalid token")
	}

	return tokenString, expiresAt, nil
}

// ExtractClaims extracts claims from given token
func ExtractClaims(tokenString string, tokenSecretKey string) (jwt.MapClaims, error) {
	var (
		token *jwt.Token
	)

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return []byte(tokenSecretKey), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		return nil, errors.Wrap(err, " invalid token")
	}

	return claims, nil
}
