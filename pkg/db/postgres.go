package db

import (
	"alzargar_backend/config"
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq" // postgres drivers
)

func ConnectDB(cfg config.Config) (*sql.DB, error) {
	str := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)
	connDB, err := sql.Open("postgres", str)
	if err != nil {
		log.Println("Failed to error connect database", err)
	}

	return connDB, nil
}


